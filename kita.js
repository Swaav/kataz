// Assesment

// console.log("display the numbers from 1 to 5")
// for (let count = 1; count <=5; count++){
//     console.log(count);
// }

console.log("Display the numbers from 1 to 20. (1, 2, 3, …,19, 20)")
for (let count = 1; count <= 20; count++) {
    console.log(count);
}




console.log("Display the even numbers from 1 to 20. (2, 4, 6, …, 18, 20)")
for (let count = 0; count < 20; count++) {
    if (count % 2 === 0) {
        console.log(count);
    }
}


console.log("Display the odd numbers from 1 to 20. (1, 3, 5, …, 17, 19)")
for (count = 0; count <= 20; count++) {
    if (count % 2 != 0) {
        console.log(count)
    }
}

console.log("Display the multiples of 5 up to 100. (5, 10, 15, …, 95, 100)")
for (count = 5; count <= 100; count++) {
    if (count % 5 == 0) {
        console.log(count)
    }
}


console.log("Display the numbers counting backwards from 20 to 1. (20, 19, 18, …, 2, 1)")
for (let count = 20; count >= 1; count--) {
    console.log(count)
}

console.log("Display the even numbers counting backwards from 20. (20, 18, 16, …, 4, 2)")
for (let count = 20; count >= 1; count--) {
    if (count % 2 === 0) {
        console.log(count)
    }
}

console.log("Display the odd numbers from 20 to 1, counting backwards. (19, 17, 15, …, 3, 1)")
for (let count = 20; count >= 1; count--)
    if (count % 2 != 0) { console.log(count) }

console.log("Display the multiples of 5, counting down from 100. (100, 95, 90, …, 10, 5)")
for (let count = 100; count >= 5; count--)
    if (count % 5 == 0) {
        console.log(count)
    }

console.log("Display the square numbers up to 100. (1, 4, 9, …, 81, 100)")
for (count = 1; count <= 100; count++) {
    if (Math.pow(count, 2) <= 100) {
        console.log(Math.pow(count, 2))
    }
}

console.log("Display the square numbers, counting down from 100. (100, 81, 64, …, 4, 1")
for (count = 100; count >= 1; count--)
    if (Math.pow(count, 2) <= 100) {
        console.log(Math.pow(count, 2))
    }